package com.example.authentication

data class UserInfo(
    var fullName: String = "",
    val imageUrl: String = ""
)

package com.example.authentication

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.EventLog
import android.util.Patterns
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_profile.*
import java.util.*
import javax.security.auth.login.LoginException


private lateinit var imageView: ImageView
private lateinit var textview:TextView
private lateinit var urledit: EditText
private lateinit var personname: EditText
private lateinit var savebtn: Button
private lateinit var logoutbtn: Button


private val auth= FirebaseAuth.getInstance()
private val db = FirebaseDatabase.getInstance().getReference("UserInfo")



class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        registerListeners()
        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo = snapshot.getValue(UserInfo::class.java) ?: return

                Glide.with(this@ProfileActivity)
                    .load(userInfo.imageUrl)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(imageView2)
                textView.text = userInfo.fullName

            }

            override fun onCancelled(error: DatabaseError) {

            }

        })





    }
    private fun init(){
        imageView = findViewById(R.id.imageView2)
        textview = findViewById(R.id.textView)
        urledit = findViewById(R.id.editTextUrl)
        personname = findViewById(R.id.editTextPersonName)
        savebtn = findViewById(R.id.buttonsave)
        logoutbtn = findViewById(R.id.buttonlogout)
    }
    private fun forgotPassword(email:EditText){
        if (email.text.toString().isEmpty()) {
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            return
        }
        Firebase.auth.sendPasswordResetEmail(email.text.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this,"Email sent.", Toast.LENGTH_SHORT).show()
                }
            }

    }
    private fun registerListeners(){
        logoutbtn.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this,LogInActivity::class.java))
            finish()
        }
        savebtn.setOnClickListener{
            val fullName: String = personname.text.toString()
            val url:String = urledit.text.toString()
            val userInfo = UserInfo(fullName,url)

            db.child(auth.currentUser?.uid!!).setValue(userInfo)

        }
        changepasswordbtn.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Change Password")
            val view = layoutInflater.inflate(R.layout.dialog_forgot_password, null)
            val email = view.findViewById<EditText>(R.id.et_email)
            builder.setView(view)
            builder.setPositiveButton("Reset", DialogInterface.OnClickListener { _, _ ->
                forgotPassword(email)
            })
            builder.setNegativeButton("close", DialogInterface.OnClickListener { _, _ ->  })
            builder.show()
        }

        btnlist.setOnClickListener{
            val intent = Intent(this,ListActivity::class.java)
            startActivity(intent)
        }


    }

}